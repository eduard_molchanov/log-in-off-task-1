<?php

const DBNAME = "log-in-off-task-1";
define("USER", "root");
define("PASS", "root");

class Db
{

    private static function connect()
    {
        return new PDO("mysql:host=localhost; dbname=" . DBNAME, USER, PASS);
    }

    // получение из таблицы всех строк
    public static function select_all($sql)
    {
        return self::connect()->query($sql)->fetchAll(PDO::FETCH_OBJ);

    }
 // получение из таблицы количества всех строк
    public static function count($sql)
    {
        return self::connect()->query($sql)->rowCount() ;
    }

    // получение из таблицы одной строки
    public static function select_row($sql)
    {
        return self::connect()->query($sql)->fetch(PDO::FETCH_OBJ);
    }

    // получение из таблицы одной строки - ассоциативный массив для Ajax
    public static function select_row_ajax($sql)
    {
        return self::connect()->query($sql)->fetch(PDO::FETCH_ASSOC);
    }

    // обновление, удаление, вставка строки таблицы
    public static function update_delete_insert($sql)
    {
        return self::connect()->exec($sql);
    }

    //  вставка строки таблицы
    public static function insert($sql)
    {
        return self::connect()->exec($sql);
    }

}
